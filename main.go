package main

import (
	"fmt"
	"github.com/olekukonko/tablewriter"
	"net"
	"os"
	"strconv"
	"time"
)
import "gopkg.in/yaml.v2"

type Tests []Test
type Test struct {
	Name string
	Ip   string
	Port int
}

type TestResult []string

func main() {
	if len(os.Args) != 2 {
		_, _ = fmt.Fprintf(os.Stdout, "Usage: porttester yaml_description (expected 1 arg, got %v)", len(os.Args))
		os.Exit(-1)
	}
	y := []byte(os.Args[1])
	tests := Tests{}
	err := yaml.Unmarshal(y, &tests)
	if err != nil {
		_, _ = fmt.Fprintf(os.Stdout, "invalid input: %v", err.Error())
		os.Exit(-1)
	}
	res := runTests(tests)
	printTests(res)
}

func printTests(tests []TestResult) {
	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"Name", "IP", "Port", "Resolved IP", "Connection OK", "Error"})
	table.SetBorders(tablewriter.Border{Left: true, Top: false, Right: true, Bottom: false})
	table.SetCenterSeparator("|")
	for _, row := range tests {
		table.Append(row)
	}
	table.Render()
}

func runTests(tests Tests) []TestResult {
	res := make([]TestResult, len(tests))
	for _, t := range tests {
		r := make(TestResult, 6)
		addr, err := net.ResolveIPAddr("ip4", t.Name)
		dest := &net.TCPAddr{}
		r[0] = t.Name
		r[1] = t.Ip
		r[2] = strconv.Itoa(t.Port)
		if err != nil {
			r[3] = "ERROR"
			r[5] = err.Error()
			dest, err = net.ResolveTCPAddr("tcp4", t.Ip+":"+strconv.Itoa(t.Port))
			if err != nil {
				r[4] = "NO"
				r[5] = err.Error()
			}
		} else {
			r[3] = addr.IP.String()
			dest, err = net.ResolveTCPAddr("tcp4", addr.IP.String()+":"+strconv.Itoa(t.Port))
			if err != nil {
				r[4] = "NO"
				r[5] = err.Error()
			}
		}
		if dest.Port != 0 {
			conn, err := net.DialTimeout("tcp4", dest.String(), time.Second*5)
			if err != nil {
				r[4] = "NO"
				r[5] = err.Error()
			} else {
				_ = conn.Close()
				r[4] = "OK"
			}
		}
		res = append(res, r)
	}
	return res
}
