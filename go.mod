module porttester

require (
	github.com/mattn/go-runewidth v0.0.4 // indirect
	github.com/olekukonko/tablewriter v0.0.1
	gopkg.in/yaml.v2 v2.2.2
)
