package main

import (
	"gopkg.in/yaml.v2"
	"testing"
)

func TestSimple(t *testing.T) {
	var tests = Tests{
		{
			Name: "www.google.com",
			Port: 80,
			Ip:   "216.58.215.36",
		},
	}
	y, err := yaml.Marshal(tests)
	if err != nil {
		t.Error(err)
	}
	println(string(y))
	res := runTests(tests)
	printTests(res)
}
